package org.selena.mobilecloud.data;

import org.hibernate.exception.ConstraintViolationException;
import org.selena.mobilecloud.security.User;
import org.selena.mobilecloud.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public class InitUsersData {

    @Autowired
    private UserRepository users;

    @PostConstruct
    public void createUsers() {

        try {
            users.save(User.create("student", "stu", "user", "student"));
            users.save(User.create("alice", "ali", "user"));
            users.save(User.create("bob", "bob", "admin", "user"));
            users.save(User.create("mark", "mar", "user"));
            users.save(User.create("anonymous", "", "user"));
        } catch ( RuntimeException ex )
        {
            if ( ex.getCause() instanceof ConstraintViolationException ) {
                // there are already users defined in the database, do nothing
            } else {
                throw ex;
            }

        }

    }

}
