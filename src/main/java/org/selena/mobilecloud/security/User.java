package org.selena.mobilecloud.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity
@Table( name="user_auth",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"name"})
)
public class User implements UserDetails {


    public static User create(String username, String password,
                                     String...authorities) {
        return new User(username, password, authorities);
    }


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
    private String password;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name="user_roles",
            joinColumns=@JoinColumn(name="user_id")
    )
    @Column(name="role")
    private Collection<String> roles = new HashSet<String>();

    public User() {}

    public User(String username, String pass,
                 String...authorities) {
        name = username;
        password = pass;
        roles.addAll(Arrays.asList(authorities));
    }

    public String getUsername() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public Collection<String> getRoles() {
        return roles;
    }

    public void setRoles(final Collection<String> roles) {
        this.roles = roles;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return createAuthorityList(getRoles());
    }

    public static List<GrantedAuthority> createAuthorityList(Collection<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(roles.size());

        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }

        return authorities;
    }

}
