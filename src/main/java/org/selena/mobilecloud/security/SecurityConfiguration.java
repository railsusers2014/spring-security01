package org.selena.mobilecloud.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.savedrequest.NullRequestCache;

@Configuration
// Setup Spring Security to intercept incoming requests to the Controllers
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


	/**
	 * This method is used to inject access control policies into Spring
	 * security to control what resources / paths / http methods clients have
	 * access to.
	 */
	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		// By default, Spring inserts a token into web pages to prevent
		// cross-site request forgery attacks.
		// See: http://en.wikipedia.org/wiki/Cross-site_request_forgery
		//
		// Unfortunately, there is no easy way with the default setup to communicate
		// these CSRF tokens to a mobile client so we disable them.
		// Don't worry, the next iteration of the example will fix this
		// problem.
		//http.csrf().disable();
		// We don't want to cache requests during login
		http.requestCache().requestCache(new NullRequestCache());



		// Allow all clients to access the login page and use
		// it to login
		http.formLogin()
			// The default login url on Spring is "j_security_check" ...
		    // which isn't very friendly. We change the login url to
		    // something more reasonable ("/login").
			.loginProcessingUrl("/login")
			.permitAll();

		// Make sure that clients can logout too!! This has to be done with POST
		http.logout()
             .deleteCookies("remove")
             .invalidateHttpSession(false)
		     // Change the default logout path to /logout
			 .logoutUrl("/logout")
			 // Allow everyone to access the logout URL
			 .permitAll();

		// We force clients to authenticate before accessing ANY URLs
		// other than the login and lougout that we have configured above.
		http.authorizeRequests().anyRequest().authenticated();
	}

	/**
	 * 
	 * This method is used to setup the users that will be able to login to the
	 * system. The users are read from the database
	 * 
	 * @param auth
	 * @throws Exception
	 */
    @Autowired
    private UserDetailsService userDetailsService;

	@Autowired
	protected void registerAuthentication(
			final AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService( userDetailsService );

	}

    /**
     * Return all of our user information to anyone in the framework who requests it.
     */
    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }

}
